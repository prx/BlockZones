#!/bin/ksh
set -x
clear
###
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/03/26
#
###

###
#
# For OpenBSD
#
###

export LC_ALL=C

IPv4=1	# enable or not IPV4 management - use by hosts, unbound
IPv6=1	# enable or not IPv6 management - use by hosts, unbound

USE_LZ_REDIRECT=0	# enable or not to use local-zone redirect for unbound

RACINE="$(dirname "$(readlink -f -- "$0")")"
DIR_DL="${RACINE}/downloads"
DIR_LISTS="${RACINE}/lists"
DIR_SRC="${RACINE}/src"

list="domains"

now="$(date +"%x %X")"
today="$(date +%s)"

seconds=86400   # in seconds

set -A blocklists

ARG="${1-ARG}"
if [[ "$ARG" = "ARG" ]]; then
    ARG="unbound"
fi

###
#
# Functions
#
###

### Get data into file in var list...
build_blocklists() {

    printf "%s \n" "*** Manage list: ${DIR_SRC}/${list} ***"

    if [ -f "${DIR_SRC}/${list}" ]; then

        i=0
        while read -r line; do
            #if [ "$(echo "${line}" | grep -v "^#")" ]; then blocklists[$i]="${line}"; fi
            if echo "${line}" | grep -v "^#"; then blocklists[$i]="${line}"; fi
            let i++
        done < "${DIR_SRC}/${list}"
        unset i

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Le fichier ${DIR_SRC}/${list} semble ne pas être lisible !"
        exit 1

    fi

    }

build_sums() {

    if [ -f "${DIR_LISTS}/${output}" ];  then

        cd "${DIR_LISTS}" || exit 1
        #sha512sum --tag "${output}" > "${output}.sha512"
        if sha512 -h "${output}.sha512" "${output}"; then
			printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The checksum file is correctly created!"
        else
			printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem to create checksum file!"
        fi
        cd "${RACINE}" || exit 1

    fi

    }

build_uniq_list() {

    # on s'assure d'une liste de noms uniques
    ## http://promberger.info/linux/2009/01/14/removing-duplicate-lines-from-a-file/

    #{ rm "${DIR_SRC}/uniq_${list}" && awk '!x[tolower($1)]++' > "${DIR_SRC}/uniq_${list}"; } < "${DIR_SRC}/uniq_${list}"
    # shellcheck disable=SC2094
    { rm "${DIR_SRC}/uniq_${list}" && awk '!x[tolower($1)]++' | sort -du -o "${DIR_SRC}/uniq_${list}"; } < "${DIR_SRC}/uniq_${list}"

    }

download() {

    printf "%s \n" "########## Attempt to get blocklists files ##########"
    printf "%s \n" "=> Attempt to download file: ${filename}"

    local bool=0

    if [ -x "$(which curl)" ]; then

        if ! "$(which curl)" -A "Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -o "${filename}" "${url}"; then
            bool=1
        fi

    elif [ -x "$(which wget)" ]; then

        if ! "$(which wget)" --user-agent="Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -c -O "${filename}" "${url}"; then
            bool=1
        fi

    else

        if ! ftp -n -m -C -o "${filename}" "${url}"; then bool=1; fi

    fi

    if [ ${bool} -eq 0 ]; then

        printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The file ${filename} is correctly downloaded!"

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem with download file ${filename}!"

    fi

    }

del_uniq_list() {

    if [ -f "${DIR_SRC}/uniq_${list}" ]; then rm "${DIR_SRC}/uniq_${list}"; fi

}

make_uniq_list() {

    if [ ! -f "${DIR_SRC}/uniq_${list}" ]; then touch "${DIR_SRC}/uniq_${list}"; fi

    #mime="text/plain"

    #if [ "$(file -b -i "${filename}")" = "${mime}" ]; then

        printf "%s \n" "====> Attempt to make uniq file with filename: ${filename}"
        # !a[$0]++
        awk '{ print tolower($0) }' "${filename}" >> "${DIR_SRC}/uniq_${list}"
        sed -i -e "/^$/d;/^[[:space:]]*$/d" "${DIR_SRC}/uniq_${list}"

    #fi

    unset mime

    }

# Create uniq list file by datas into array blocklist
mng_blocklists() {

    printf "%s \n" "### Essai de lecture des données blocklist"

    count="${#blocklists[@]}"

    if [ "${count}" -gt 0 ]; then

        for url in "${blocklists[@]}"; do

			if [ "${url}" = "personals" ]; then
				filename="${DIR_SRC}/${url}"
			else
				ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
				file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
				name="${ndd}_${file}"
				filename="${DIR_DL}/${name}"
			fi
            #printf "file: %s \n" "${file}"

            # define seconds before new dl
            case "${ndd}" in
                "mirror1.malwaredomains.com") seconds=2592000;;   # 1 month
                "winhelp2002.mvps.org") seconds=604800;; # 7 days
                "www.spamhaus.org") seconds=3600;; # 1 hours
                #*) seconds=86400;;
            esac

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${today} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then download; fi

            else

                download

            fi

            uncompress

            case "${ndd}" in
                "hosts-file.net")
                    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
                        filename="${filename%.zip}/hosts.txt"
                    fi
                ;;
                "mirror1.malwaredomains.com")
                    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
                        case "${file}" in
                            "immortal_domains.zip")
                                filename="${filename%.zip}/${file%.zip}.txt"
                            ;;
                            "justdomains.zip"|"malwaredomains.zones.zip")
                                filename="${filename%.zip}/${file%.zip}"
                            ;;
                        esac
                    fi
                ;;
                "winhelp2002.mvps.org")
                    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
                        filename="${filename%.zip}/HOSTS"
                    fi
                ;;
            esac

            purge_files

            make_uniq_list

            unset filename

        done

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Il semble ne pas y avoir de données récupérées !"
        exit 1

    fi

    unset count

    }

purge_files() {

    printf "%s \n" "===> Attempt to transform downloaded file: ${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one

    case "${ndd}" in
        "adaway.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^$/d;/^#/d;s/\(.*\) ##\(.*\)#/\1/g;/\(.*\)localhost\(.*\)/d;" "${filename}"
                ;;
                *)
                    sed -i -e "/^$/d;/^#/d;s/\(.*\) ##\(.*\)#/\1/g;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\ \(.*\)#\1#g;" "${filename}"
                ;;
            esac
        ;;
        "hosts-file.net")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s/ \+//g" "${filename}"
                ;;
                *)
                    sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\t\(.*\)#\1#g;s/ \+//g" "${filename}"
                ;;
            esac
        ;;
        "mirror1.malwaredomains.com")
            case "${file}" in
                "immortal_domains.zip")
                    sed -i -e "/^#/d;/^notice/d;s/ \+//g" "${filename}"
                ;;
                "malwaredomains.zones.zip")
                    #sed -i -e "/^\/\//d;s/ \+/ /g;s#zone \"\(.*\)\" {type master; file \"/etc/namedb/blockeddomain.hosts\";};#\1#g" "${filename}"
                    { rm "${filename}" && sed -e "/^\/\//d;s/ \+/ /g" | awk '{print $2}' | sed -e "s/\"//g" | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "someonewhocares.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^#/d;/^\t\+#/d;s/^[ \t]*//g;/^ \+#/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d" "${filename}"
                ;;
                *)
                    #sed -i -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d;s#127.0.0.1 \(.*\)#\1#g" "${filename}"
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "winhelp2002.mvps.org")
            case "${ARG}" in
                "host0")
                    sed -i -e "/^#/d;/^$/d;/^\r/d;/\(.*\)localhost\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
                ;;
                *)
                    sed -i -e "/^#/d;/^$/d;/^\r/d;/\(.*\)localhost\(.*\)/d;s#0.0.0.0 \(.*\)#\1#g;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
                ;;
            esac
        ;;
        "www.malwaredomainlist.com")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    #sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" "${filename}"
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
                *)
                    #sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1  \(.*\)#\1#g" "${filename}" #;
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

    #cat "${filename}" | tr -d '[:space:]' > "${filename}"

    }

transformer() {

    printf "%s \n" "===> Attempt to transform uniq list '${DIR_SRC}/uniq_${list}' in list '${ARG}'..."

    case "${ARG}" in
        "bind"|"bind8"|"bind9")
            format="Bind Config"
            output="bind.zone"
        ;;
        "host"|"hosts"|"host0")
            format="hosts"
            output="hosts"
        ;;
        "unbound")
            format="Local zone"
            output="local-zone"
        ;;
        "pf")
			format="Packet-Filter"
			output="baddomains"
        ;;
    esac

    if [ -f "${DIR_SRC}/uniq_${list}" ];  then

        mssg="###########################
### Block Zones Project ###
###########################
### Format '${format}'
### Date : ${now}
##
#"

        echo "${mssg}" > "${DIR_LISTS}/${output}"
        unset mssg
        
        case "${ARG}" in
			"host0")
                echo "0.0.0.0 localhost" >> "${DIR_LISTS}/${output}"
             ;;
             "host"|"hosts")
				if [ "${IPv4}" = 1 ]; then echo "127.0.0.1 localhost" >> "${DIR_LISTS}/${output}"; fi
				if [ "${IPv6}" = 1 ]; then echo "::1 localhost" >> "${DIR_LISTS}/${output}"; fi
             ;;
        esac

        i=0
        while read -r line; do
            line="$(echo "${line}" | tr -d '[:space:]')"    # replace '[:space:]' by '\040\011\012\015' for oldier version

            case "${ARG}" in
                "bind8")
                    echo "zone \"${line}\" { type master; notify no; file \"null.zone.file\"; };" >> "${DIR_LISTS}/${output}"
                ;;
                "bind"|"bind9")
                    echo "zone \"${line}\" { type master; notify no; file \"/etc/bind/nullzonefile.txt\"; };" >> "${DIR_LISTS}/${output}"
                ;;
                "host0")
                    echo "0.0.0.0 ${line}" >> "${DIR_LISTS}/${output}"
                ;;
                "host"|"hosts")
                    if [ "${IPv4}" = 1 ]; then echo "127.0.0.1 ${line}" >> "${DIR_LISTS}/${output}"; fi
                    if [ "${IPv6}" = 1 ]; then echo "::1 ${line}" >> "${DIR_LISTS}/${output}"; fi
                ;;
                "pf")
					echo "${line}"  >> "${DIR_LISTS}/${output}"
                ;;
                "unbound")
                    if [ "${IPv4}" = 1 ]; then 
						if [ "${USE_LZ_REDIRECT}" = 1 ]; then
							echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}"; 
						else
							echo -e "local-data: \"${line} A 127.0.0.1\"" >> "${DIR_LISTS}/${output}";
						fi
					fi
                    if [ "${IPv6}" = 1 ]; then 
						if [ "${USE_LZ_REDIRECT}" = 1 ]; then
							echo -e "local-zone: \"${line}\" redirect\nlocal-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}"; 
						else
							echo -e "local-data: \"${line} AAAA ::1\"" >> "${DIR_LISTS}/${output}"; 
						fi
					fi
                ;;
            esac

            let i++
        done  < "${DIR_SRC}/uniq_${list}"
        unset i

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Il semble ne pas y avoir de fichier '${DIR_SRC}/uniq_${list}' !"
        exit 1

    fi

    unset arg

    if [ -f "${DIR_LISTS}/${output}" ]; then

        printf "[ \\33[1;32m%s\\33[0;39m ] %s \n" "OK" "Le fichier '${DIR_LISTS}/${output}' semble avoir été construit !"

        build_sums

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Il semble ne pas y avoir de fichier '${DIR_LISTS}/${output}' !"
        exit 1

    fi

    }

uncompress() {

    if [ "$(file -b -i "${filename}")" = "application/gzip" ]; then
        printf "%s \n" "==> Attempt to extract archive .gz: ${filename}"
        gunzip -d -f -q "${filename}";
    fi

    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
        printf "%s \n" "==> Attempt to extract archive .zip: ${filename}"
        unzip -oqu "${filename}" -d "${filename%.zip}"
    fi

    }

#verify_uid
#verify_need_dirs

del_uniq_list

build_blocklists

mng_blocklists

build_uniq_list

transformer
