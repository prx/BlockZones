BlockZone
=========

Projet pour blacklister des noms de domaines, et des adresses ip, *connus pour leur activité suspecte*, relatifs :
- serveurs ADS - publicitaires
- malwares, trackers, et autres "méchancetés".
- réseaux "bogons"
- et autres "badips" ...

/!\ **Janvier 2017 :: ATTENTION: Les noms des scripts ont été renommés en les raccourcissant, par suppression des caractères 'mng\_' ... !!!** /!\

----------

Script 'badips'
--------------

=> Le script 'badips' agit en plusieurs temps :

- il télécharge les listes de fichiers enregistrés dans le fichier 'src/badips', en tenant compte d'un certain délai de latence afin de ne pas trop revisiter les sites enregistrés
- il les traitent pour créer deux listes uniques, une 'list/badips\_ipv4' et une autre 'list/badips\_ipv6' ... qui sont recréées à chaque lancement du script, avec leur propre fichier de somme de contrôle sha512.

Copiez les fichiers où vous voulez qu'ils soient traités par pf !

=> règles pf ipv4 :

    table <t_badips> persist file "/dir/badips_ipv4"
    
    block drop in quick on egress from { <t_badips> } to any
    block drop out quick on egress from any to { <t_badips> }
    
=> règles pf ipv6 :
    
    table <t_badips6> persist file "/dir/badips_ipv6"
    
    block drop in quick on egress inet6 from { <t_badips6> } to any
    block drop out quick on egress inet6 from any to { <t_badips6> }

Ce script n'existe qu'en version ksh, pour OpenBSD.

/!\ Pensez à créer un fichier cron **quotidien**, hebdomaire, ou mensuel pour mettre-à-jour les informations, puis recharger pf. /!\

**Attention** : Il semble nécessaire d'agrandir le nombre des entrées de tables ... si vous voulez utiliser toutes les adresses retournées.

----------

Script 'blacklist'
----------------

=> Le script 'blacklist' agit en plusieurs temps :

- il télécharge les listes de fichiers autorisés dans le fichier 'src/domains', en tenant compte d'un délai de latence pour certains qui n'acceptent pas qu'on les revisitent trop souvent, au cas où le script serait relancé plusieurs fois dans la journée. Il suffit de les décommenter pour que le script 'blacklists' les gèrent.
- il les traitent pour créer une liste unique 'lists/uniq_domains' ... qui est récrée à chaque lancement du script.
- puis, il créé un fichier :
  - 'lists/local-zone' pour traitement par unbound,
  - 'lists/bind.zone' pour traitement par bind (version 8, et 9),
  - ou, 'lists/hosts' pour un traitement local par fichier hosts ...
  - voire 'lists/baddomains' pour un traitement par tables dans PF - pour la version OpenBSD. /!\ Attention, étant donné les temps de traitement par PF pour convertir les noms de domaines en adresses ip ... c'est plus une preuve de concept ; d'autant si PF n'est pas capable de résoudre un nom de domaine correctement, dans ce cas PF n'acceptera pas de traiter la liste. Préfèrez l'usage des listes pour hosts, voire unbound /!\
  - ainsi que leur propre fichier de somme de contrôle sha512 !
  
Depuis Janvier 2017 : À-propos de la version 'unbound', la variable 'USE_LZ_REDIRECT' sert à gèrer si vous voulez l'ajout de la mention 'local-zone "adr_ip" redirect" ... 

**ATTENTION** : Si vous activez toutes les URLs référencées dans le fichier 'lists/domains', il est possible que le service devant traiter la liste unique finale ne puisse le faire par manque de ressources mémoires.
De même, cela augmentera le temps de traitement et de création de la liste unique par votre machine.

Ce script existe en version bash - *pour Linux, de préférence Debian* -, et en version ksh - *pour OpenBSD*.

/!\ Pensez à créer un fichier de cron quotidien, **hebdomadaire** ou mensuel pour mettre-à-jour les informations, puis recharger pf ... il existe une version pour OBSD dans le répertoire 'cron'. /!\

=> Le fichier 'lists/personals' existe pour enregistrer vos propres choix de restrictions de domaines - un par ligne.


----------

Script 'bogons'
---------------

=> Le script 'bogons' récupère les listes de bogons ipv4 et/ou ipv6 mises-à-disposition par la Team Cymru. Il les traite pour que ce soit fonctionnel avec pf - packet filter.
Une fois le script exécuté, retrouvez la liste traitée dans le répertoire 'lists/', avec son fichier de sommes de contrôle sha512.
- 'fullbogons-ipv4.txt' pour ipv4
- 'fullbogons-ipv6.txt' pour ipv6

**Version KSH (OpenBSD)**
Copiez-le fichier où vous voulez pour qu'il puisse être "exécuté" par pf.

**Attention** : Il peut être nécessaire d'agrandir le nombre des entrées de tables !

=> règles pf ipv4 :

    table <t_bogons> persist file "/dir/fullbogons-ipv4.txt"
    
    block drop in quick on egress from { <t_bogons> } to any
    block drop out quick on egress from any to { <t_bogons> }
    
=> règles pf ipv6 :
    
    table <t_bogons6> persist file "/dir/fullbogons-ipv6.txt"
    
    block drop in quick on egress inet6 from { <t_bogons6> } to any
    block drop out quick on egress inet6 from any to { <t_bogons6> }

**Version Bash (Linux)**

À vous de gèrer ... pour le faire fonctionner avec iptables !

Tel que - *ce qui suit est un exemple* - :

    while read -r line; do
		/sbin/iptables -I INPUT -s "${line}" -j DROP
		/sbin/iptables -I OUTPUT -d "${line}" -j DROP
    done < /dir/BlockZones/lists/fullbogons-ipv4.txt
    
*Note : Faites de même pour la liste bogons ipv6 :*

    while read -r line; do
		/sbin/ip6tables -I INPUT -s "${line}" -j DROP
		/sbin/ip6tables -I OUTPUT -d "${line}" -j DROP
    done < /dir/BlockZones/lists/fullbogons-ipv6.txt

**Autres informations :**

/!\ Pensez à créer un fichier cron mensuel pour mettre-à-jour les informations, puis recharger pf. /!\

----------

Création de listes
------------------

./blacklist options : pour créer une liste ...

Les options sont :

- 'unbound', pour le service 'unbound'
- 'bind8', bind9', pour le service 'Bind'
- 'hosts', pour le fichier /etc/hosts - ou son équivalent selon l'OS.

La configuration par défaut du fichier 'lists/domains' suffit pour être gérée correctement par des services comme 'unbound'. Si vous cherchez à gérer l'ensemble des urls, vous aurez le droit à des messages de dépassements de mémoire - ce qui signifie qu'il ne peut gérer l'ensemble de la liste que vous aurez créée !
<br/>
Ce "problème" ne se pose pas avec la gestion du fichiers 'hosts'.

La configuration par défaut gère près de 65000 bad urls. La version complète gère un peu moins de 500000 !

**Retrouvez ces listes mises-à-jours tous les jours, avec leurs fichiers de sommes de contrôle sha512, à l'adresse suivante :** <br/>
[https://stephane-huc.net/share/BlockZones/lists/][1]

Listes gérées
-------------

- les listes 'immortals domains', et/ou 'malwaredomains' - Initiative DNS-BH Malwaredomains.com
- les listes HpHosts - hosts-file.net : **attention l'usage automatique est strictement interdit !**
- les listes Abuse - abuse.ch
- la liste malwaredomainlist - malwaredomainlist.com
- la liste winhelp2002 MVPS  - winhelp2002.mvps.org
- la liste pgl yoyo - pgl.yoyo.org
- la liste adaway - adaway.org
- la liste Dan Pollock - someonewhocares.org

----------

Mémo PF
-------

Juste quelques infos pour mémos, à-propos de Packet-Filter, utiles dans ce contexte !

/!\ Pensez à créer une tâche régulière pour vider les tables pf, au besoin /!\

    # pfctl -t table_name -T expire nb_seconds

=> recharger une des tables - on recharge PF :

    # pfctl -f /etc/pf.conf

=> augmenter le nombre d'entrées de table - éditer '/etc/pf.conf' - c'est juste un exemple - :

    set limit table-entries 300000
   
=> Si vous utilisez les listes bogons, et badips, pensez à optimisez vos règles PF, tel que, par exemple :

    block drop in quick on egress from { <t_badips>, <t_bogons> } to any
    block drop out quick on egress from any to { <t_badips>, <t_bogons> }

Et, idem pour les règles ipv6, bien sûr !

----------

[1]: https://stephane-huc.net/share/BlockZones/lists/
